/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGEH__
#define __MY1IMAGEH__
/*----------------------------------------------------------------------------*/
/* 00core */
#include "my1image_base.h"
#include "my1image_gray.h"
#include "my1image_argb.h"
#include "my1image_crgb.h"
/* 00file */
#include "my1image_file.h"
#include "my1image_file_bmp.h"
#include "my1image_file_pnm.h"
#include "my1image_file_png.h"
/* 00full */
#include "my1image_chsv.h"
#include "my1image_draw.h"
/* 01core */
#include "my1image_area.h"
#include "my1image_mask.h"
/* 01util */
#include "my1image_buff.h"
#include "my1image_util.h"
/* 01work */
#include "my1image_scan.h"
#include "my1image_mono.h"
#include "my1image_math.h"
/* 01xtra + 00full => 01full */
#include "my1image_stat.h"
#include "my1image_ifdb.h"
#include "my1image_ifdb_base.h"
#if defined(__MY1IMAGE_DO_APPW__)
/* 05core */
#include "my1image_task.h"
#include "my1image_view.h"
#include "my1image_appw.h"
#endif
#if defined(__MY1IMAGE_DO_MAIN__)
/* 05main */
#include "my1image_grab.h"
#include "my1image_main.h"
#endif
#if defined(__MY1IMAGE_DO_LIVE__)
/* 05live */
#include "my1libav_grab.h"
#endif
#if defined(__MY1IMAGE_DO_DEMO__)
/* 05full */
#include "my1image_hist.h"
#include "my1image_data.h"
#endif
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGEH__ */
/*----------------------------------------------------------------------------*/
