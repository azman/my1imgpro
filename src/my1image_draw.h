/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_DRAWH__
#define __MY1IMAGE_DRAWH__
/*----------------------------------------------------------------------------*/
#include "my1image_argb.h"
/*----------------------------------------------------------------------------*/
#define DEFAULT_DRAW_COLOR RGB_WHITE
#define DEFAULT_FILL_COLOR RGB_BLACK
/*----------------------------------------------------------------------------*/
typedef struct {
	my1image_t *view;
	int dpen, fpen; /* draw (fg) pen color and fill (bg) pen color */
	/* temporary storage */
	int xdif, ydif, xcur, ycur, xdir, ydir;
	int temp, swap, step, loop;
} my1image_draw_t;
/*----------------------------------------------------------------------------*/
typedef my1image_draw_t my1idraw_t;
/*----------------------------------------------------------------------------*/
void image_draw_init(my1idraw_t* draw, my1image_t* view);
void image_draw_line(my1idraw_t* draw, int x1, int y1, int x2, int y2);
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_DRAWH__ */
/*----------------------------------------------------------------------------*/
