/*----------------------------------------------------------------------------*/
#include "my1image_05live.h"
/*----------------------------------------------------------------------------*/
#define WFLAG_SHOW_ORIGINAL 0x01
#define WFLAG_VIDEO_MODE 0x10
#define WFLAG_VIDEO_LIVE (WFLAG_VIDEO_MODE|0x20)
/*----------------------------------------------------------------------------*/
typedef struct _my1idata_t {
	my1libav_grab_t vget;
	my1image_appw_t awin;
	my1image_t buff, *orig;
	char *list; /* filter list from args */
	int flag, tdel;
} my1idata_t;
/*----------------------------------------------------------------------------*/
int init_data(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1idata_t *what = (my1idata_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	libav1_init(&what->vget,&mdat->load);
	image_appw_init(&what->awin);
	image_init(&what->buff);
	what->orig = 0x0;
	what->list = 0x0;
	what->flag = 0;
	what->tdel = 0;
	return 0;
}
/*----------------------------------------------------------------------------*/
int free_data(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1idata_t *what = (my1idata_t*)task->data;
	image_free(&what->buff);
	image_appw_free(&what->awin);
	libav1_free(&what->vget);
	return 0;
}
/*----------------------------------------------------------------------------*/
int igrab_grab_video(void* data, void* that, void* xtra) {
	my1itask_t* ptemp = (my1itask_t*) data;
	my1image_grab_t* igrab = (my1image_grab_t*) that;
	my1idata_t *what = (my1idata_t*)ptemp->data;
	my1libav_grab_t* vgrab = (my1libav_grab_t*)&what->vget;
	if (!vgrab->flag) {
		if ((what->flag&WFLAG_VIDEO_LIVE)==WFLAG_VIDEO_LIVE)
			libav1_live(vgrab,igrab->pick);
		else
			libav1_file(vgrab,igrab->pick);
		if (!vgrab->flag)
			igrab->flag |= IGRAB_FLAG_LOAD_ERROR;
	}
	libav1_grab(vgrab,igrab->grab);
	return igrab->flag;
}
/*----------------------------------------------------------------------------*/
int args_data(void* data, void* that, void* xtra) {
	int loop, argc, *temp = (int*) that;
	char** argv = (char**) xtra;
	my1itask_t *task = (my1itask_t*)data;
	my1idata_t *what = (my1idata_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)task->temp;
	my1igrab_t *grab = (my1igrab_t*)&mdat->grab;
	if (mdat->flag&IFLAG_ERROR) return mdat->flag;
	/* re-check parameter for video option */
	argc = *temp; loop = 1;
	if (argc>2) {
		if (!grab->pick)
			grab->pick = argv[loop];
		if (!strncmp(grab->pick,"--video",8)) {
			grab->pick = argv[++loop];
			what->flag |= WFLAG_VIDEO_MODE;
		}
		else if (!strncmp(grab->pick,"--live",7)) {
			grab->pick = argv[++loop];
			what->flag |= WFLAG_VIDEO_LIVE;
		}
	}
	/* setup grabber */
	if (what->flag&WFLAG_VIDEO_MODE) {
		grab->do_grab.task = igrab_grab_video;
		grab->do_grab.data = (void*)what;
		grab->grab = &mdat->load;
		mdat->flag |= IFLAG_VIDEO_MODE;
	}
	/* continue with the rest */
	for (++loop;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--original",10))
			what->flag |= WFLAG_SHOW_ORIGINAL;
		else if (!strncmp(argv[loop],"--filter",9))
			what->list = argv[++loop];
		else if (!strncmp(argv[loop],"--timed",9))
			what->tdel = atoi(argv[++loop]);
		else
			printf("-- Unknown param '%s'!\n",argv[loop]);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int prep_data(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1idata_t *what = (my1idata_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	my1ifdb_t* ifdb;
	/** load all predefined filters */
	if (!mdat->list) {
		ifdb = (my1ifdb_t*) malloc(sizeof(my1ifdb_t));
		if (ifdb) {
			ifdb_init(ifdb);
			ifdb_generate_base(ifdb);
			mdat->list = ifdb_make_all(ifdb);
			ifdb_free(ifdb);
		}
	}
	/** check requested filters */
	if (what->list) {
		char* pchk = what->list;
		int loop = 0, curr = 0, stop = 0;
		while (!stop) {
			switch (pchk[loop]) {
				case 0x0: stop = 1;
				case ',':
					pchk[loop] = 0x0;
					printf("-- Loading filter '%s'... ",&pchk[curr]);
					if (imain_filter_doload(mdat,&pchk[curr]))
						printf("OK!\n");
					else printf("not found?!\n");
					if (!stop) pchk[loop] = ',';
					curr = ++loop;
					break;
				default: loop++;
			}
		}
	}
	/* prep timed display - only valid for picture mode */
	if (!(what->flag&WFLAG_VIDEO_MODE)) {
		if (what->tdel>0)
			mdat->info.tdel = what->tdel;
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
int exec_data(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1idata_t *what = (my1idata_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	if (what->flag&WFLAG_SHOW_ORIGINAL)
		what->orig = mdat->view;
	image_copy(&what->buff,mdat->view);
	mdat->view = &what->buff;
	return 0;
}
/*----------------------------------------------------------------------------*/
int what_on_keychk(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1imain_t *mdat = (my1imain_t*)task->temp;
	GdkEventKey *event = (GdkEventKey*)xtra;
	if (event->keyval == GDK_KEY_z)
		imain_menu_filter_enable(mdat,!(mdat->flag&IFLAG_FILTER_EXE));
	else if (event->keyval == GDK_KEY_space)
		mdat->flag ^= IFLAG_VIDEO_HOLD;
	return 0;
}
/*----------------------------------------------------------------------------*/
int show_data(void* data, void* that, void* xtra) {
	my1itask_t *task = (my1itask_t*)data;
	my1idata_t *what = (my1idata_t*)task->data;
	my1imain_t *mdat = (my1imain_t*)that;
	if (what->orig) {
		my1image_show_t show = { what->orig, "Source Image", 0, 0};
		/* show original */
		image_appw_show(&what->awin,&show);
		/* modify name for main win */
		image_appw_name(&mdat->iwin,"Processed Image");
	}
	if (!(mdat->flag&IFLAG_VIDEO_MODE))
		image_appw_domenu(&mdat->iwin);
	imain_domenu_filters(mdat);
	image_appw_domenu_quit(&mdat->iwin);
	if (what->flag&WFLAG_VIDEO_MODE) {
		itask_make(&mdat->iwin.keychk,what_on_keychk,(void*)what);
		mdat->iwin.keychk.temp = (void*) mdat;
		imain_loop(mdat,0);
	}
	return 0;
}
/*----------------------------------------------------------------------------*/
void idata_main(my1idata_t* what, my1imain_t* that) {
	imain_data(that,what);
	that->init.task = init_data;
	that->free.task = free_data;
	that->args.task = args_data;
	that->prep.task = prep_data;
	that->proc.task = exec_data;
	that->show.task = show_data;
}
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1idata_t data;
	my1imain_t that;
	idata_main(&data,&that);
	imain_init(&that);
	imain_args(&that,argc,argv);
	imain_prep(&that);
	imain_proc(&that);
	imain_show(&that,argc,argv);
	imain_free(&that);
	return 0;
}
/*----------------------------------------------------------------------------*/
