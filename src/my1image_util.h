/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_UTILH__
#define __MY1IMAGE_UTILH__
/*----------------------------------------------------------------------------*/
#include "my1image_buff.h"
/*----------------------------------------------------------------------------*/
#define FILTER_NAMESIZE 80
/*----------------------------------------------------------------------------*/
struct _my1image_filter_t;
/*----------------------------------------------------------------------------*/
typedef my1image_t* (*pfilter_t)(my1image_t* curr, my1image_t* next,
	struct _my1image_filter_t* filter);
typedef void (*pfsetup_t)(struct _my1image_filter_t* filter,
	struct _my1image_filter_t* pclone);
typedef void (*pfclean_t)(struct _my1image_filter_t* filter);
typedef void (*pfdocfg_t)(struct _my1image_filter_t* filter,
	char *pkey, char *pval);
/*----------------------------------------------------------------------------*/
#define FILTER_FLAG_NONE 0x00
/* auto-grayscale input on single pass! */
#define FILTER_FLAG_GRAY 0x01
/* marker for cloned filter */
#define FILTER_FLAG_COPY 0x08
/* flag to skip filter (filter disable bit - system level) */
#define FILTER_FLAG_SKIP 0x10
/* marker for multi-step filter (non-first) */
#define FILTER_FLAG_MORE 0x20
/* flag to skip multi-step filter */
#define FILTER_FLAG_MOFF 0x40
/* flag to skip filter (filter disable bit - process level) */
#define FILTER_FLAG_COFF 0x80
/*----------------------------------------------------------------------------*/
#define FILTER_FLAG_NOEXEC (FILTER_FLAG_SKIP|FILTER_FLAG_COFF)
/*----------------------------------------------------------------------------*/
/* user-defined flags - guaranteed to be NOT used by the library */
#define FILTER_FLAG_USR0 0x1000
#define FILTER_FLAG_USR1 0x2000
#define FILTER_FLAG_USR2 0x4000
#define FILTER_FLAG_USR3 0x8000
/*----------------------------------------------------------------------------*/
#define FILTER_FLAG_ERROR (~(~0U>>1))
#define FILTER_FLAG_ERRORCONF (FILTER_FLAG_ERROR|0x00010000)
/*----------------------------------------------------------------------------*/
typedef struct _my1image_filter_t {
	char name[FILTER_NAMESIZE];
	unsigned int uuid, flag; /* unique id & properties flag (1-hot) */
	pfilter_t filter; /* pointer to filter function */
	pfsetup_t doinit; /* pointer to setup function */
	pfclean_t dofree; /* pointer to cleanup function */
	pfdocfg_t doconf; /* pointer to config function */
	my1ibuff_t *buffer; /* external shared buffer */
	my1image_t *output; /* output image to write to */
	void *data; /* filter-specific data */
	struct _my1image_filter_t *next, *last; /* linked list - last in list */
	struct _my1image_filter_t *prev; /* ONLY FOR PROCESSING! */
} my1image_filter_t;
/*----------------------------------------------------------------------------*/
typedef my1image_filter_t my1ipass_t;
/*----------------------------------------------------------------------------*/
void filter_init(my1ipass_t* pass, pfilter_t filter, char *name);
void filter_free(my1ipass_t* pass);
void filter_free_clones(my1ipass_t* pass);
my1ipass_t* filter_insert(my1ipass_t* pass, my1ipass_t* next);
my1ipass_t* filter_remove(my1ipass_t* pass, int index, int cloned);
my1ipass_t* filter_search(my1ipass_t* pass, char *name);
/* make malloc'ed filter structure */
my1ipass_t* filter_make(char* name, pfilter_t);
my1ipass_t* filter_idfl(my1ipass_t* pass,unsigned int uuid, unsigned int flag);
my1ipass_t* filter_conf(my1ipass_t* pass, pfsetup_t, pfclean_t, pfdocfg_t);
my1ipass_t* filter_doinit(my1ipass_t* pass, my1ipass_t* from);
my1ipass_t* filter_cloned(my1ipass_t* pass);
/* apply filter on image */
my1image_t* image_filter(my1image_t* data, my1ipass_t* pass);
my1image_t* image_filter_single(my1image_t* data, my1ipass_t* pass);
/*----------------------------------------------------------------------------*/
/* short-named macros for nested ones */
#define ifc1(a,b) filter_make(a,b)
#define ifc2(a,b,c) filter_idfl(a,b,c)
#define ifc3(a,b,c,d) filter_conf(a,b,c,d)
#define ifc4(a,b) filter_doinit(a,b)
/*----------------------------------------------------------------------------*/
/* creator macros */
#define filter_create(lbl,uid,flg,fun,fn1,fn0) \
	ifc4(ifc3(ifc2(ifc1(lbl,fun),uid,flg),fn1,fn0,0x0),0x0)
#define filter_create_conf(lbl,uid,flg,fun,fn1,fn0,fnc) \
	ifc4(ifc3(ifc2(ifc1(lbl,fun),uid,flg),fn1,fn0,fnc),0x0)
/* this DOES NOT run doinit! */
#define filter_create_base(lbl,uid,flg,fun,fn1,fn0,fnc) \
	ifc3(ifc2(ifc1(lbl,fun),uid,flg),fn1,fn0,fnc)
/*----------------------------------------------------------------------------*/
/* useful macros */
#define filter_doskip(pf) ((my1ipass_t*)pf)->flag |= FILTER_FLAG_SKIP
#define filter_noskip(pf) ((my1ipass_t*)pf)->flag &= ~FILTER_FLAG_SKIP
#define filter_domore(pf) ((my1ipass_t*)pf)->flag |= FILTER_FLAG_MORE
#define filter_doself(pf) ((my1ipass_t*)pf)->flag &= ~FILTER_FLAG_MORE
#define filter_multi0(pf) ((my1ipass_t*)pf)->flag |= FILTER_FLAG_MOFF
#define filter_multi1(pf) ((my1ipass_t*)pf)->flag &= ~FILTER_FLAG_MOFF
#define filter_noexec(pf) ((my1ipass_t*)pf)->flag |= FILTER_FLAG_COFF
#define filter_doexec(pf) ((my1ipass_t*)pf)->flag &= ~FILTER_FLAG_COFF
#define filter_cfgerr(pf) ((my1ipass_t*)pf)->flag |= FILTER_FLAG_ERRORCONF
#define filter_clrerr(pf) ((my1ipass_t*)pf)->flag &= ~FILTER_FLAG_ERRORCONF
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_UTILH__ */
/*----------------------------------------------------------------------------*/
