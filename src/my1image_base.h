/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_BASEH__
#define __MY1IMAGE_BASEH__
/*----------------------------------------------------------------------------*/
typedef struct _my1image_t {
	int *data; /* 1-d vector for 2-d image */
	int size; /* memory is cheap - precalculate this! */
	int mask; /* general flag - can store rgb-encoded int! */
	int cols,rows;
} my1image_t;
/*----------------------------------------------------------------------------*/
void image_init(my1image_t *image);
void image_free(my1image_t *image);
int* image_make(my1image_t *image, int rows, int cols);
void image_copy(my1image_t *dst, my1image_t *src);
void image_fill(my1image_t *image, int value);
/*----------------------------------------------------------------------------*/
/* useful macros => col(x),row(y) */
#define image_get_pixel(i,r,c) \
	((my1image_t*)i)->data[(int)(r)*(((my1image_t*)i)->cols)+(c)]
#define image_set_pixel(i,r,c,p) \
	((my1image_t*)i)->data[(int)(r)*(((my1image_t*)i)->cols)+(c)] = p
#define image_row_data(i,r) \
	&((my1image_t*)i)->data[(int)(r)*(((my1image_t*)i)->cols)]
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_BASEH__ */
/*----------------------------------------------------------------------------*/
