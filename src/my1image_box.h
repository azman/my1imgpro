/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_BOX_H__
#define __MY1IMAGE_BOX_H__
/*----------------------------------------------------------------------------*/
/**
	my1image_box.h
	- nice-to-have data structure for image filters
	  = easier to manipulate size (e.g. padding)
	  = independent of my1image_area
	- written by azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
typedef struct _my1image_box_t {
	int xl,xr,yt,yb; /* left,right,top,bottom */
} my1image_box_t;
/*----------------------------------------------------------------------------*/
typedef my1image_box_t my1ibox_t;
/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_AREAH__
typedef struct _my1image_rect_t {
	int xset, yset;
	int wval, hval;
} my1image_rect_t;
#else
typedef my1image_area_t my1image_rect_t;
#endif
/*----------------------------------------------------------------------------*/
typedef my1image_rect_t my1rect_t;
/*----------------------------------------------------------------------------*/
void ibox_init(my1ibox_t* pbox);
void ibox_copy(my1ibox_t* pbox, my1ibox_t* from);
void ibox_fill(my1ibox_t* pbox, int xl, int xr, int yt, int yb);
void ibox_padding(my1ibox_t* pbox, int mpad);
void ibox_padx(my1ibox_t* pbox, int padx);
void ibox_pady(my1ibox_t* pbox, int pady);
int ibox_valid(my1ibox_t* pbox);
int ibox_within(my1ibox_t* pbox, my1ibox_t* view);
int ibox_overlapped(my1ibox_t* pbox, my1ibox_t* view);
int ibox_contain(my1ibox_t* pbox, int xchk, int ychk);
void ibox_make_rect(my1ibox_t* pbox, my1rect_t* rect);
void ibox_from_rect(my1ibox_t* pbox, my1rect_t* rect);
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_BOX_H__ */
/*----------------------------------------------------------------------------*/
