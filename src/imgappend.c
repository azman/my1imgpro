/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "my1image_file.h"
#include "my1image_file_png.h"
#include "my1image_area.h"
#include "my1image_crgb.h"
/*----------------------------------------------------------------------------*/
#define FLAG_INVERT_IMG 0x0001
#define FLAG_WHITE_FILL 0x0002
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test, loop, stat, flag, vert;
	int offs, skip, ends, pads, icol, irow;
	my1image_t buf1, buf2, full;
	my1image_area_t pick;
	char odef_name[] = "imgappend.bmp";
	char *inp1, *inp2, *out0;
	image_file_init();
	/* init image structures */
	image_init(&buf1);
	image_init(&buf2);
	image_init(&full);
	do { /* dummy loop */
		stat = 0; skip = 2; /* default use 2 border pixels */
		vert = 0; offs = 0; ends = 0; pads = 0; flag = 0;
		if (argc<3) {
			fprintf(stderr,"** Need at least 2 input images!\n");
			stat--;
			break;
		}
		out0 = odef_name;
		inp1 = argv[1];
		inp2 = argv[2];
		for (loop=3;loop<argc;loop++) {
			if (!strncmp(argv[loop],"-o",3)) {
				if (++loop==argc) {
					fprintf(stderr,"** No data for '%s'!\n",argv[loop-1]);
					stat--;
				}
				else out0 = argv[loop];
			}
			/* trench between */
			else if (!strncmp(argv[loop],"-s",3)) {
				if (++loop==argc) {
					fprintf(stderr,"** No data for '%s'!\n",argv[loop-1]);
					stat--;
				}
				else {
					test = atoi(argv[loop]);
					if (test<1)
						fprintf(stderr,"** Invalid value for border '%s'!\n",
							argv[loop]);
					else skip = test;
				}
			}
			/* margin@padding - init */
			else if (!strncmp(argv[loop],"-m",3)) {
				if (++loop==argc) {
					fprintf(stderr,"** No data for '%s'!\n",argv[loop-1]);
					stat--;
				}
				else {
					test = atoi(argv[loop]);
					if (test<0)
						fprintf(stderr,"** Invalid value for i-pad '%s'!\n",
							argv[loop]);
					else offs = test;
				}
			}
			/* margin@padding - ends */
			else if (!strncmp(argv[loop],"-e",3)) {
				if (++loop==argc) {
					fprintf(stderr,"** No data for '%s'!\n",argv[loop-1]);
					stat--;
				}
				else {
					test = atoi(argv[loop]);
					if (test<0)
						fprintf(stderr,"** Invalid value for e-pad '%s'!\n",
							argv[loop]);
					else ends = test;
				}
			}
			/* tangent padding */
			else if (!strncmp(argv[loop],"-t",3)) {
				if (++loop==argc) {
					fprintf(stderr,"** No data for '%s'!\n",argv[loop-1]);
					stat--;
				}
				else {
					test = atoi(argv[loop]);
					if (test<0)
						fprintf(stderr,"** Invalid value for t-pad '%s'!\n",
							argv[loop]);
					else pads = test;
				}
			}
			else if (!strncmp(argv[loop],"-v",3)) vert = 1;
			else if (!strncmp(argv[loop],"--inv",6)) flag |= FLAG_INVERT_IMG;
			else if (!strncmp(argv[loop],"--fillw",8)) flag |= FLAG_WHITE_FILL;
			else fprintf(stderr,"** Unknown option '%s'!\n",argv[loop]);
		}
		if (stat) break;
		/* load image 1 */
		if ((test=image_load(&buf1,inp1))<0) {
			fprintf(stderr,"** Error loading image 1 '%s'! [%x]\n",
				inp1,(unsigned)test);
			stat--;
			break;
		}
		/* load image 2 */
		if ((test=image_load(&buf2,inp2))<0) {
			fprintf(stderr,"** Error loading image 2 '%s'! [%x]\n",
				inp2,(unsigned)test);
			stat--;
			break;
		}
		printf("-- Img1: %s\n",inp1);
		printf("   Size: %d x %d (Size:%d) {Mask:%08X}\n",
			buf1.cols,buf1.rows,buf1.size,buf1.mask);
		printf("-- Img2: %s\n",inp2);
		printf("   Size: %d x %d (Size:%d) {Mask:%08X}\n",
			buf2.cols,buf2.rows,buf2.size,buf2.mask);
		if (flag&FLAG_INVERT_IMG) {
			image_invert(&buf1);
			image_invert(&buf2);
		}
		if (vert) {
			icol = (buf1.cols>buf2.cols?buf1.cols:buf2.cols) + pads*2;
			/* append vertically */
			irow = buf1.rows + buf2.rows + skip + offs + ends;
			image_make(&full,irow,icol);
			if (flag&FLAG_WHITE_FILL)
				image_fill(&full,WHITE);
			if (buf1.mask)
				image_colormode(&full);
			/*copy in */
			pick.yset = offs;
			pick.xset = pads;
			pick.hval = buf1.rows;
			pick.wval = buf1.cols;
			image_set_area(&full,&buf1,&pick);
			pick.yset += buf1.rows + skip;
			pick.hval = buf2.rows;
			pick.wval = buf2.cols;
			image_set_area(&full,&buf2,&pick);
		}
		else {
			irow = (buf1.rows>buf2.rows?buf1.rows:buf2.rows) + pads*2;
			/* append horizontally */
			icol = buf1.cols + buf2.cols + skip + offs + ends;
			image_make(&full,irow,icol);
			if (flag&FLAG_WHITE_FILL)
				image_fill(&full,WHITE);
			if (buf1.mask)
				image_colormode(&full);
			/*copy in */
			pick.yset = pads;
			pick.xset = offs;
			pick.hval = buf1.rows;
			pick.wval = buf1.cols;
			image_set_area(&full,&buf1,&pick);
			pick.xset += buf1.cols + skip;
			pick.hval = buf2.rows;
			pick.wval = buf2.cols;
			image_set_area(&full,&buf2,&pick);
		}
		/* save image */
		if ((test=image_save(&full,out0))<0) {
			fprintf(stderr,"** Error saving image '%s'! [%x]\n",
				out0,(unsigned)test);
			stat--;
			break;
		}
		printf("-- Full: %s\n",out0);
		printf("   Size: %d x %d (Size:%d) {Mask:%08X}\n",
			full.cols,full.rows,full.size,full.mask);
	} while (0);
	/* free image structures */
	image_free(&buf1);
	image_free(&buf2);
	image_free(&full);
	image_file_free();
	return stat;
}
/*----------------------------------------------------------------------------*/
