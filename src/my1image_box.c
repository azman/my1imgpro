/*----------------------------------------------------------------------------*/
#include "my1image_box.h"
/*----------------------------------------------------------------------------*/
void ibox_init(my1ibox_t* pbox) {
	pbox->xl = -1;
	pbox->xr = -1;
	pbox->yt = -1;
	pbox->yb = -1;
}
/*----------------------------------------------------------------------------*/
void ibox_copy(my1ibox_t* pbox, my1ibox_t* from) {
	pbox->xl = from->xl;
	pbox->xr = from->xr;
	pbox->yt = from->yt;
	pbox->yb = from->yb;
}
/*----------------------------------------------------------------------------*/
void ibox_fill(my1ibox_t* pbox, int xl, int xr, int yt, int yb) {
	pbox->xl = xl;
	pbox->xr = xr;
	pbox->yt = yt;
	pbox->yb = yb;
}
/*----------------------------------------------------------------------------*/
void ibox_padding(my1ibox_t* pbox, int mpad) {
	pbox->xl -= mpad;
	pbox->xr += mpad;
	pbox->yt -= mpad;
	pbox->yb += mpad;
}
/*----------------------------------------------------------------------------*/
void ibox_padx(my1ibox_t* pbox, int padx) {
	pbox->xl -= padx;
	pbox->xr += padx;
}
/*----------------------------------------------------------------------------*/
void ibox_pady(my1ibox_t* pbox, int pady) {
	pbox->yt -= pady;
	pbox->yb += pady;
}
/*----------------------------------------------------------------------------*/
int ibox_valid(my1ibox_t* pbox) {
	if (pbox->xl<0||pbox->xr<0||pbox->yt<0||pbox->yb<0)
		return 0;
	return 1;
}
/*----------------------------------------------------------------------------*/
int ibox_within(my1ibox_t* pbox, my1ibox_t* view) {
	if (pbox->yt<view->yt||pbox->yb>view->yb||
			pbox->xl<view->xl||pbox->xr>view->xr)
		return 0;
	return 1;
}
/*----------------------------------------------------------------------------*/
#define pick_max(a,b) a<b?b:a
#define pick_min(a,b) a>b?b:a
#define ibox_area(pbox) ((pbox->xr-pbox->xl+1)*(pbox->yb-pbox->yt+1))
/*----------------------------------------------------------------------------*/
int ibox_overlapped(my1ibox_t* pbox, my1ibox_t* view) {
	int x1, x2, y1, y2;
	x1 = pick_max(pbox->xl,view->xl);
	x2 = pick_min(pbox->xr,view->xr);
	x1 = x2 - x1 + 1;
	if (x1<2) return 0;
	y1 = pick_max(pbox->yt,view->yt);
	y2 = pick_min(pbox->yb,view->yb);
	y1 = y2 - y1 + 1;
	if (y1<2) return 0;
	return (x1*y1)*100/ibox_area(pbox);
}
/*----------------------------------------------------------------------------*/
int ibox_contain(my1ibox_t* pbox, int xchk, int ychk) {
	if (ychk<pbox->yt||ychk>pbox->yb||xchk<pbox->xl||xchk>pbox->xr)
		return 0;
	return 1;
}
/*----------------------------------------------------------------------------*/
void ibox_make_rect(my1ibox_t* pbox, my1rect_t* rect) {
	rect->xset = pbox->xl;
	rect->yset = pbox->yt;
	rect->wval = pbox->xr - pbox->xl + 1;
	rect->hval = pbox->yb - pbox->yt + 1;
}
/*----------------------------------------------------------------------------*/
void ibox_from_rect(my1ibox_t* pbox, my1rect_t* rect) {
	pbox->xl = rect->xset;
	pbox->yt = rect->yset;
	pbox->xr = pbox->xl + rect->wval - 1;
	pbox->yb = pbox->yt + rect->hval - 1;
}
/*----------------------------------------------------------------------------*/
