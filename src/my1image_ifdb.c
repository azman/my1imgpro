/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_IFDBC__
#define __MY1IMAGE_IFDBC__
/*----------------------------------------------------------------------------*/
#include "my1image_ifdb.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void ifdb_init(my1ifdb_t* ifdb) {
	ifdb->list = 0x0;
	ifdb->size =0;
	/**ifdb->temp =0;**/
}
/*----------------------------------------------------------------------------*/
void ifdb_free(my1ifdb_t* ifdb) {
	my1ipass_t* temp;
	while (ifdb->list) {
		temp = ifdb->list;
		ifdb->list = ifdb->list->next;
		filter_free(temp);
		free((void*)temp);
		ifdb->size--;
	}
}
/*----------------------------------------------------------------------------*/
my1ipass_t* ifdb_pass(my1ifdb_t* ifdb, my1ipass_t* that) {
	if (that)
		ifdb->list = filter_insert(ifdb->list,that);
	return that;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* ifdb_find(my1ifdb_t* ifdb, char* name) {
	my1ipass_t *find, *temp;
	find = 0x0;
	temp = ifdb->list;
	while (temp) {
		if (!strncmp(temp->name,name,strlen(temp->name))) {
			find = temp;
			break;
		}
		temp = temp->next;
	}
	return find;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* ifdb_make(my1ifdb_t* ifdb, char* name) {
	my1ipass_t *that, *temp;
	that = 0x0;
	temp = ifdb_find(ifdb,name);
	if (temp) that = filter_cloned(temp);
	return that;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* ifdb_make_all(my1ifdb_t* ifdb) {
	my1ipass_t *list, *that, *temp;
	list = 0x0;
	temp = ifdb->list;
	while (temp) {
		that = filter_cloned(temp);
		if (that) list = filter_insert(list,that);
		temp = temp->next;
	}
	return list;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_IFDBC__ */
/*----------------------------------------------------------------------------*/
