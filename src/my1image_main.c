/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_MAIN_C__
#define __MY1IMAGE_MAIN_C__
/*----------------------------------------------------------------------------*/
#include <string.h>
/*----------------------------------------------------------------------------*/
#include "my1image_main.h"
#include "my1image_file.h"
/*----------------------------------------------------------------------------*/
void imain_data(my1imain_t* imain, pdata_t data) {
	itask_make(&imain->init,0x0,data);
	itask_make(&imain->free,0x0,data);
	itask_make(&imain->args,0x0,data);
	itask_make(&imain->prep,0x0,data);
	itask_make(&imain->proc,0x0,data);
	itask_make(&imain->show,0x0,data);
	imain->data = data;
}
/*----------------------------------------------------------------------------*/
void imain_init(my1imain_t* imain) {
	image_init(&imain->load);
	imain->view = 0x0;
	image_file_init();
	image_appw_init(&imain->iwin);
	image_show_prep(&imain->info,0x0,DEFAULT_WINDOW_LABEL);
	imain->info.flag |= IMAGE_SHOW_FLAG_QUIT;
	igrab_init(&imain->grab);
	buffer_init(&imain->buff);
	imain->flag = IFLAG_OK;
	imain->stat = 0;
	imain->tdel = MY1IMAIN_LOOP_DELAY;
	/*imain->temp = 0;*/
	imain->dovf = 0x0;
	imain->list = 0x0;
	imain->curr = 0x0;
	itask_exec(&imain->init,(void*)imain,0x0);
}
/*----------------------------------------------------------------------------*/
void imain_free(my1imain_t* imain) {
	itask_exec(&imain->free,(void*)imain,0x0);
	if (imain->curr) filter_free_clones(imain->curr);
	if (imain->list) filter_free_clones(imain->list);
	buffer_free(&imain->buff);
	igrab_free(&imain->grab);
	image_appw_free(&imain->iwin);
	image_file_free();
	image_free(&imain->load);
}
/*----------------------------------------------------------------------------*/
void imain_args(my1imain_t* imain, int argc, char* argv[]) {
	if (argc>1&&argv[1][0]!='-') {
		imain->grab.pick = argv[1];
		imain->grab.grab = &imain->load;
	}
	imain->args.temp = (void*)imain;
	itask_exec(&imain->args,(void*)&argc,(void*)argv);
	igrab_grab_default(&imain->grab);
}
/*----------------------------------------------------------------------------*/
void imain_prep(my1imain_t* imain) {
	if (imain->flag&IFLAG_ERROR) return;
	if (!imain->grab.pick) {
		image_make(&imain->load,DEF_HEIGHT,DEF_WIDTH);
		image_fill(&imain->load,BLACK);
		imain->view = &imain->load;
	}
	else {
		igrab_grab(&imain->grab);
		if (imain->grab.flag&IGRAB_FLAG_ERROR) {
			imain->flag |= IFLAG_ERROR_LOAD;
			return;
		}
		imain->view = imain->grab.grab;
	}
	itask_exec(&imain->prep,(void*)imain,0x0);
}
/*----------------------------------------------------------------------------*/
void imain_proc(my1imain_t* imain) {
	if (imain->flag&IFLAG_ERROR) return;
	imain_filter_doexec(imain);
	itask_exec(&imain->proc,(void*)imain,0x0);
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_execute(my1imain_t *imain, GtkMenuItem *menu_item) {
	/* this is exclusive for non-video mode! */
	if (imain->curr) {
		imain->flag |= IFLAG_FILTER_RUN;
		while (imain->flag&IFLAG_FILTER_CHK);
		/** running filter on displayed image */
		image_appw_pass_filter(&imain->iwin,imain->curr);
		imain->flag &= ~IFLAG_FILTER_RUN;
	}
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_clear(my1imain_t *imain, GtkMenuItem *menu_item) {
	imain_filter_unload_all(imain);
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_doskip(my1ipass_t *check, GtkMenuItem *menu_item) {
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menu_item)))
		check->flag &= ~FILTER_FLAG_SKIP;
	else
		check->flag |= FILTER_FLAG_SKIP;
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_unload(my1imain_t *imain, GtkMenuItem *menu_item) {
	char* name = (char*)gtk_menu_item_get_label(menu_item);
	while (name[0]!=' ') { name++; } name++;
	imain_filter_unload(imain,name);
}
/*----------------------------------------------------------------------------*/
void imain_menu_filter_enable(my1imain_t *imain, int enable) {
	gboolean able = FALSE;
	GtkMenuItem *menu_item = (GtkMenuItem*)imain->dovf;
	if (!menu_item) return;
	if (enable) able = TRUE;
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item),able);
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_toggle(my1imain_t *imain, GtkMenuItem *menu_item) {
	if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(menu_item)))
		imain->flag |= IFLAG_FILTER_EXE;
	else
		imain->flag &= ~IFLAG_FILTER_EXE;
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_load(my1imain_t* imain) {
	GtkWidget *doopen = gtk_file_chooser_dialog_new("Open Filter List",
		GTK_WINDOW(imain->iwin.window),GTK_FILE_CHOOSER_ACTION_OPEN,
		"_Cancel", GTK_RESPONSE_CANCEL,
		"_Open", GTK_RESPONSE_ACCEPT, NULL);
	if (gtk_dialog_run(GTK_DIALOG(doopen))==GTK_RESPONSE_ACCEPT) {
		gchar *filename, *buff;
		filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(doopen));
		if (imain_filter_doload_file(imain,(char*)filename))
			buff = g_strdup_printf("[CHECK] '%s' loaded!",filename);
		else
			buff = g_strdup_printf("[ERROR] Cannot load '%s'!",filename);
		printf("-- %s\n",(char*)buff);
		g_free(buff);
		g_free(filename);
	}
	gtk_widget_destroy(doopen);
}
/*----------------------------------------------------------------------------*/
void imain_domenu_current(my1imain_t *imain, GtkMenuItem *menu_item) {
	int flag;
	my1ipass_t *temp;
	gchar *buff;
	GList *curr_list, *next_list;
	GtkWidget *menu_main, *menu_temp, *menu_exec, *menu_clra, *menu_subs;
	GtkWidget *menu_chk1, *menu_chk2;
	menu_main = gtk_menu_item_get_submenu(menu_item);
	/* remove all items? */
	curr_list = gtk_container_get_children(GTK_CONTAINER(menu_main));
	for (next_list=curr_list;next_list!=NULL;next_list=next_list->next)
		gtk_widget_destroy(GTK_WIDGET(next_list->data));
	g_list_free(curr_list);
	if (!(imain->flag&IFLAG_VIDEO_MODE)) {
		/* create executor */
		menu_exec = gtk_menu_item_new_with_label("Execute");
		g_signal_connect_swapped(G_OBJECT(menu_exec),"activate",
			G_CALLBACK(imain_on_filter_execute),(gpointer)imain);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_exec);
		gtk_widget_set_sensitive(menu_exec,FALSE);
		gtk_widget_show(menu_exec);
	}
	else menu_exec = 0x0;
	/* reset filter */
	menu_clra = gtk_menu_item_new_with_label("Clear All");
	g_signal_connect_swapped(G_OBJECT(menu_clra),"activate",
		G_CALLBACK(imain_on_filter_clear),(gpointer)imain);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_clra);
	gtk_widget_set_sensitive(menu_clra,FALSE);
	gtk_widget_show(menu_clra);
	/* filter load menu */
	menu_temp = gtk_menu_item_new_with_label("Load...");
	g_signal_connect_swapped(G_OBJECT(menu_temp),"activate",
		G_CALLBACK(imain_on_filter_load),(gpointer)imain);
	gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_temp);
	gtk_widget_show(menu_temp);
	/* separator */
	menu_temp = gtk_separator_menu_item_new();
	gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_temp);
	gtk_widget_show(menu_temp);
	/* create new items */
	temp = imain->curr; flag = 0;
	while (temp) {
		menu_subs = gtk_menu_new();
		menu_chk1 = gtk_check_menu_item_new_with_label("Enabled");
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_subs),menu_chk1);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_chk1),
			temp->flag&FILTER_FLAG_SKIP?FALSE:TRUE);
		g_signal_connect_swapped(G_OBJECT(menu_chk1),"activate",
			G_CALLBACK(imain_on_filter_doskip),(gpointer)temp);
		gtk_widget_show(menu_chk1);
		buff = g_strdup_printf("Remove %s",temp->name);
		menu_chk2 = gtk_menu_item_new_with_label(buff);
		g_free(buff);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_subs),menu_chk2);
		g_signal_connect_swapped(G_OBJECT(menu_chk2),"activate",
			G_CALLBACK(imain_on_filter_unload),(gpointer)imain);
		gtk_widget_show(menu_chk2);
		buff = g_strdup_printf("%d-%s",flag,temp->name);
		menu_temp = gtk_menu_item_new_with_label(buff);
		g_free(buff);
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_temp),menu_subs);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_temp);
		gtk_widget_show(menu_temp);
		temp = temp->next;
		flag++;
	}
	if (flag) {
		if (menu_exec)
			gtk_widget_set_sensitive(menu_exec,TRUE);
		gtk_widget_set_sensitive(menu_clra,TRUE);
	}
}
/*----------------------------------------------------------------------------*/
void imain_on_filter_select(my1imain_t *imain, GtkMenuItem *menu_item) {
	char* name = (char*)gtk_menu_item_get_label(menu_item);
	imain_filter_doload(imain,name);
}
/*----------------------------------------------------------------------------*/
void imain_domenu_filters(my1imain_t* imain) {
	my1ipass_t *temp;
	GtkWidget *menu_main, *menu_item, *menu_subs, *menu_temp;
	if (imain->iwin.domenu) menu_main = imain->iwin.domenu;
	else {
		menu_main = gtk_menu_new();
		imain->iwin.domenu = menu_main;
		gtk_widget_show(menu_main);
	}
	temp = imain->list; menu_subs = 0x0;
	while (temp) {
		if (!menu_subs) menu_subs = gtk_menu_new();
		menu_item = gtk_menu_item_new_with_label(temp->name);
		g_signal_connect_swapped(G_OBJECT(menu_item),"activate",
			G_CALLBACK(imain_on_filter_select),(gpointer)imain);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_subs),menu_item);
		gtk_widget_show(menu_item);
		temp = temp->next;
	}
	if (menu_subs) {
		/* temp menu to insert as sub-menu */
		menu_temp = gtk_menu_item_new_with_label("Filter Listed");
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_temp),menu_subs);
		/* add to main menu */
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_temp);
		gtk_widget_show(menu_temp);
		/* create sub menu for current filter? */
		menu_subs = gtk_menu_new();
		menu_temp = gtk_menu_item_new_with_label("Filter Picked");
		gtk_menu_item_set_submenu(GTK_MENU_ITEM(menu_temp),menu_subs);
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_temp);
		g_signal_connect_swapped(G_OBJECT(menu_temp),"activate",
			G_CALLBACK(imain_domenu_current),(gpointer)imain);
		gtk_widget_show(menu_temp);
	}
	if (imain->flag&IFLAG_VIDEO_MODE) {
		menu_item = gtk_check_menu_item_new_with_label("Filtered");
		gtk_menu_shell_append(GTK_MENU_SHELL(menu_main),menu_item);
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item),
			imain->flag&IFLAG_FILTER_EXE?TRUE:FALSE);
		g_signal_connect_swapped(G_OBJECT(menu_item),"activate",
			G_CALLBACK(imain_on_filter_toggle),(gpointer)imain);
		gtk_widget_show(menu_item);
		imain->dovf = (void*) menu_item;
	}
}
/*----------------------------------------------------------------------------*/
void imain_show(my1imain_t* imain,int argc, char* argv[]) {
	if (imain->flag&IFLAG_ERROR) return;
	gtk_init(&argc,&argv);
	if (imain->view) {
/** do not need this?
		my1ipass_t* test = 0x0;
		if (!(imain->flag&IFLAG_VIDEO_MODE)) {
			int save = imain->flag & IFLAG_FILTER_EXE;
			imain->flag |= IFLAG_FILTER_EXE;
			test = imain_filter_doexec(imain);
			if (!save) imain->flag &= ~IFLAG_FILTER_EXE;
		}
*/
		imain->info.buff = imain->view;
		image_appw_show(&imain->iwin,&imain->info);
		/* ALWAYS get a copy of original image? */
		image_copy(imain->iwin.orig,&imain->load);
		/* replace original image on display - if filtered */
		/**if (test) image_copy(imain->iwin.orig,&imain->load);*/
	}
	itask_exec(&imain->show,(void*)imain,0x0);
	if (imain->flag&IFLAG_ERROR) return;
	gtk_main();
}
/*----------------------------------------------------------------------------*/
int imain_on_task_timer(void* data, void* that, void* xtra) {
	my1imain_t* imain = (my1imain_t*) data;
	my1iappw_t* iappw = (my1iappw_t*) that;
	if (!(imain->flag&IFLAG_VIDEO_HOLD)) {
		igrab_grab(&imain->grab);
		imain->view = imain->grab.grab;
		imain_proc(imain);
		image_appw_make(iappw,imain->view);
	}
	image_appw_task(iappw,imain_on_task_timer,imain->tdel);
	return 0;
}
/*----------------------------------------------------------------------------*/
void imain_loop(my1imain_t* imain, int delta_ms) {
	if (delta_ms>0) imain->tdel = delta_ms;
	if (imain->iwin.task.task) return;
	imain->iwin.task.data = (void*) imain;
	image_appw_task(&imain->iwin,imain_on_task_timer,imain->tdel);
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_dolist(my1imain_t* imain, my1ipass_t* ichk) {
	my1ipass_t* temp = filter_search(imain->list,ichk->name);
	if (temp) {
		imain->flag |= IFLAG_ERROR_LIST1;
		return 0x0;
	}
	temp = filter_cloned(ichk);
	if (temp) imain->list = filter_insert(imain->list,temp);
	else imain->flag |= IFLAG_ERROR_LIST2;
	return temp;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* ipass_filter_doconf(my1ipass_t* pass, char *conf) {
	char *pchk, *pkey, *pval;
	int loop, curr, stop, iequ;
	pchk = conf; pkey = 0x0;
	loop = 0; curr = 0; stop = 0;
	/* skip first ':' */
	while (pchk[0]&&pchk[0]!=':') pchk++;
	if (!pchk[0]) stop = 1;
	/* find config */
	while (!stop) {
		switch (pchk[loop]) {
			case 0x0: stop = 1;
			case ':':
				pchk[loop] = 0x0;
				pkey = &pchk[curr];
				iequ = 0;
				while (pkey[iequ]&&pkey[iequ]!='=') iequ++;
				if (iequ<(loop-curr)) {
					pval = &pkey[iequ+1];
					pkey[iequ] = 0x0;
					if (pass->doconf)
						pass->doconf(pass,pkey,pval);
					/**printf("@@ Debug:{key:%s}{val:%s}\n",pkey,pval);*/
				}
				if (!stop) pchk[loop] = ':';
				curr = ++loop;
				break;
			default: loop++;
		}
	}
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_doload(my1imain_t* imain, char* name) {
	char *conf, link;
	my1ipass_t *find, *temp;
	conf = strchr(name,(int)':');
	if (conf) conf[0] = 0x0;
	link = name[0];
	if (link=='>') name++;
	find = filter_search(imain->list,name);
	temp = find ? filter_cloned(find) : 0x0;
	if (temp) {
		if (link=='>'&&imain->curr) filter_domore(temp);
		imain->flag |= IFLAG_FILTER_CHK;
		while (imain->flag&IFLAG_FILTER_RUN);
		temp->buffer = &imain->buff;
		imain->curr = filter_insert(imain->curr,temp);
		/**printf("@@ Debug:{pass:%s}\n",temp->name);*/
		if (conf) {
			conf[0] = ':';
			ipass_filter_doconf(temp,conf);
		}
		imain->flag &= ~IFLAG_FILTER_CHK;
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_unload_all(my1imain_t* imain) {
	my1ipass_t* temp = imain->curr;
	if (temp) {
		imain->flag |= IFLAG_FILTER_CHK;
		while (imain->flag&IFLAG_FILTER_RUN);
		filter_free_clones(imain->curr);
		imain->curr = 0x0;
		imain->flag &= ~IFLAG_FILTER_CHK;
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_unload(my1imain_t* imain, char* name) {
	int size, loop = 0;
	my1ipass_t* pass = imain->curr, *temp = 0x0;
	/* if name is null, remove all! */
	if (!name) return imain_filter_unload_all(imain);
	imain->flag |= IFLAG_FILTER_CHK;
	while (imain->flag&IFLAG_FILTER_RUN);
	while (pass) {
		size = strlen(pass->name) + 1;
		if (!strncmp(pass->name,name,size)) {
			imain->curr = filter_remove(imain->curr,loop,1);
			temp = pass;
			break;
		}
		loop++;
		pass = pass->next;
	}
	imain->flag &= ~IFLAG_FILTER_CHK;
	return temp;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_doload_list(my1imain_t* imain, char* csfn) {
	my1ipass_t *load, *done = 0x0;
	int loop = 0, curr = 0, stop = 0;
	/* load filters based on comma-separated-filter-name given */
	while (!stop) {
		switch (csfn[loop]) {
			case 0x0: stop = 1;
			case ',':
				csfn[loop] = 0x0;
				if ((load=imain_filter_doload(imain,&csfn[curr])))
					done = load;
				if (!stop) csfn[loop] = ',';
				curr = ++loop;
				break;
			default: loop++;
		}
	}
	return done; /* a valid pointer if at least ONE is loaded */
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_doload_file(my1imain_t* imain, char* name) {
	char fname[FILTER_NAMESIZE*2], *pname;
	my1ipass_t *load, *done;
	FILE* pfile;
	done = 0x0;
	pfile = fopen(name,"rt");
	if (pfile) {
		while (fgets(fname,FILTER_NAMESIZE*2,pfile)) {
			pname = fname;
			while (*pname) {
				if (pname[0]=='\n'||pname[0]==' '||
						pname[0]=='\t'||pname[0]=='\r') {
					pname[0] = 0x0;
				}
				pname++;
			}
			if ((load=imain_filter_doload(imain,fname)))
				done = load;
		}
		fclose(pfile);
	}
	return done;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* imain_filter_doexec(my1imain_t* imain) {
	my1ipass_t* done = 0x0;
	if ((imain->flag&IFLAG_FILTER_EXE)&&imain->curr) {
		imain->flag |= IFLAG_FILTER_RUN;
		while (imain->flag&IFLAG_FILTER_CHK);
		imain->view = image_filter(imain->view,imain->curr);
		done = imain->curr;
		imain->flag &= ~IFLAG_FILTER_RUN;
	}
	return done;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_MAIN_C__ */
/*----------------------------------------------------------------------------*/
