/*----------------------------------------------------------------------------*/
#include "my1image_main.h"
#include "my1image_data.h"
#include "my1image_file.h"
#include "my1image_file_png.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define DEF_WIDTH 320
#define DEF_HEIGHT 240
/*----------------------------------------------------------------------------*/
#define MY1APP_NAME "my1image_test"
#define MY1APP_INFO "MY1Image Test Program"
#ifndef MY1APP_VERS
#define MY1APP_VERS "build"
#endif
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1idata_t data;
	my1imain_t that;
	/* print tool info */
	printf("\n%s - %s (%s)\n",MY1APP_NAME,MY1APP_INFO,MY1APP_VERS);
	printf("  => by azman@my1matrix.org\n\n");
	/* work it! */
	idata_main(&data,&that);
	imain_init(&that);
	imain_args(&that,argc,argv);
	imain_prep(&that);
	imain_proc(&that);
	imain_show(&that,argc,argv);
	imain_free(&that);
	putchar('\n');
	return 0;
}
/*----------------------------------------------------------------------------*/
