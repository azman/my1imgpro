/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_FILEH__
#define __MY1IMAGE_FILEH__
/*----------------------------------------------------------------------------*/
#include "my1image_file_fmt.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1image_file_t {
	unsigned int flag, rsv0;
	my1image_format_t *init, *last;
} my1image_file_t;
/*----------------------------------------------------------------------------*/
void image_file_init(void);
void image_file_free(void);
void image_file_ifmt(my1image_format_t* ifmt);
/*----------------------------------------------------------------------------*/
my1image_format_t* ifmt_make(unsigned int,char*,pimgfile,pimgfile);
/*----------------------------------------------------------------------------*/
void image_format_insert(my1image_format_t* imgfmt);
/*----------------------------------------------------------------------------*/
int image_load(my1image_t *image, char *filename);
int image_save(my1image_t *image, char *filename);
int image_cdat(my1image_t *image, char *filename); /* in c-array format */
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_FILEH__ */
/*----------------------------------------------------------------------------*/
