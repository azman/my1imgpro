/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_DRAWC__
#define __MY1IMAGE_DRAWC__
/*----------------------------------------------------------------------------*/
#include "my1image_draw.h"
/*----------------------------------------------------------------------------*/
void image_draw_init(my1idraw_t* draw, my1image_t* view) {
	draw->view = view;
	draw->dpen = DEFAULT_DRAW_COLOR;
	draw->fpen = DEFAULT_DRAW_COLOR;
}
/*----------------------------------------------------------------------------*/
void image_draw_line(my1idraw_t* draw, int x1, int y1, int x2, int y2) {
	/* should i check boundaries? */
	draw->ydif = y2 - y1;
	if (draw->ydif<0) { draw->ydif = -draw->ydif; draw->ydir = -1; }
	else draw->ydir = 1;
	draw->xdif = x2 - x1;
	if (draw->xdif<0) { draw->xdif = -draw->xdif; draw->xdir = -1; }
	else draw->xdir = 1;

	if (draw->ydif > draw->xdif) {
		draw->temp = draw->xdif;
		draw->xdif = draw->ydif;
		draw->ydif = draw->temp;
		draw->swap=1;
	}
	else draw->swap=0;

	draw->xcur = x1;
	draw->ycur = y1;
	draw->step = 2 * draw->ydif - draw->xdif;
	for (draw->loop=0; draw->loop<=draw->xdif; draw->loop++) {
		image_set_pixel(draw->view,draw->ycur,draw->xcur,draw->dpen);
		while (draw->step>=0) {
			if (draw->swap==1) draw->xcur += draw->xdir;
			else draw->ycur += draw->ydir;
			draw->step -= 2*draw->xdif;
		}
		if (draw->swap==1) draw->ycur += draw->ydir;
		else draw->xcur += draw->xdir;
		draw->step += 2*draw->ydif;
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_DRAWC__ */
/*----------------------------------------------------------------------------*/
