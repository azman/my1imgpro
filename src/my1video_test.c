/*----------------------------------------------------------------------------*/
#include "my1video_data.h"
#include "my1image_main.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
/*----------------------------------------------------------------------------*/
#define MY1APP_NAME "my1video_test"
#define MY1APP_INFO "MY1Video Test Program"
#ifndef MY1APP_VERS
#define MY1APP_VERS "build"
#endif
/*----------------------------------------------------------------------------*/
static char showkeys[] = {
	"\t---------------------------\n"
	"\tDefined Keys:\n"
	"\t---------------------------\n"
	"\tc       - play video\n"
	"\ts       - stop video\n"
	"\t<SPACE> - pause/play video\n"
	"\t]       - next frame\n"
	"\t[       - previous frame\n"
	"\tl       - toggle looping\n"
	"\tz       - toggle filter\n"
	"\tq@<ESC> - quit program\n"
	"\t---------------------------\n"
};
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	my1vdata_t vdat;
	my1imain_t vapp;
	printf("\n%s - %s (%s)\n",MY1APP_NAME,MY1APP_INFO,MY1APP_VERS);
	printf("  => by azman@my1matrix.org\n");
	vdata_main(&vdat,&vapp);
	imain_init(&vapp);
	imain_args(&vapp,argc,argv);
	imain_prep(&vapp);
	imain_proc(&vapp);
	if (!(vapp.flag&IFLAG_ERROR))
		printf("\n%s\n",showkeys);
	imain_show(&vapp,argc,argv);
	imain_free(&vapp);
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
