/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_IFDBH__
#define __MY1IMAGE_IFDBH__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
/*----------------------------------------------------------------------------*/
/* extensible db! */
typedef struct _my1ifdb_t {
	my1ipass_t *list;
	int size, temp;
} my1ifdb_t;
/*----------------------------------------------------------------------------*/
void ifdb_init(my1ifdb_t* ifdb);
void ifdb_free(my1ifdb_t* ifdb);
/* that MUST BE malloc'ed image filter - will be managed by ifdb */
my1ipass_t* ifdb_pass(my1ifdb_t* ifdb, my1ipass_t* that);
my1ipass_t* ifdb_find(my1ifdb_t* ifdb, char* name);
my1ipass_t* ifdb_make(my1ifdb_t* ifdb, char* name);
my1ipass_t* ifdb_make_all(my1ifdb_t* ifdb);
/*----------------------------------------------------------------------------*/
#define ifdb_append(pdb,lbl,uid,flg,fun,fn1,fn0) \
	ifdb_pass(pdb,ifc4(ifc3(ifc2(ifc1(lbl,fun),uid,flg),fn1,fn0,0x0),0x0))
#define ifdb_append_full(pdb,lbl,uid,flg,fun,fn1,fn0,fnc) \
	ifdb_pass(pdb,ifc4(ifc3(ifc2(ifc1(lbl,fun),uid,flg),fn1,fn0,fnc),0x0))
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_IFDBH__ */
/*----------------------------------------------------------------------------*/
