/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_BUFFC__
#define __MY1IMAGE_BUFFC__
/*----------------------------------------------------------------------------*/
#include "my1image_buff.h"
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
void buffer_init(my1ibuff_t* ibuff) {
	image_init(&ibuff->main);
	image_init(&ibuff->buff);
	image_init(&ibuff->xtra);
	image_area_make(&ibuff->region,0,0,0,0);
	image_area_make(&ibuff->select,0,0,0,0);
	ibuff->curr = &ibuff->main;
	ibuff->next = &ibuff->buff;
	/** ibuff->temp = 0x0; / * not necessary */
	ibuff->iref = 0x0;
	ibuff->data = 0x0;
	ibuff->size = 0;
	ibuff->flag = 0;
}
/*----------------------------------------------------------------------------*/
void buffer_free(my1ibuff_t* ibuff) {
	if (ibuff->data)
		free(ibuff->data);
	image_free(&ibuff->xtra);
	image_free(&ibuff->buff);
	image_free(&ibuff->main);
}
/*----------------------------------------------------------------------------*/
void buffer_size(my1ibuff_t* ibuff, int height, int width) {
	image_make(&ibuff->main,height,width);
	image_make(&ibuff->buff,height,width);
}
/*----------------------------------------------------------------------------*/
void buffer_size_all(my1ibuff_t* ibuff, int height, int width) {
	image_make(&ibuff->main,height,width);
	image_make(&ibuff->buff,height,width);
	image_make(&ibuff->xtra,height,width);
}
/*----------------------------------------------------------------------------*/
void buffer_swap(my1ibuff_t* ibuff) {
	ibuff->temp = ibuff->curr;
	ibuff->curr = ibuff->next;
	ibuff->next = ibuff->temp;
}
/*----------------------------------------------------------------------------*/
void buffer_data_make(my1ibuff_t* ibuff, unsigned int size) {
	void *temp;
	if (size&&size>ibuff->size) {
		temp = realloc(ibuff->data,size);
		if (temp) {
			ibuff->data = temp;
			ibuff->size = size;
		}
	}
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_BUFFC__ */
/*----------------------------------------------------------------------------*/
