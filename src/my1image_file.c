/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_FILEC__
#define __MY1IMAGE_FILEC__
/*----------------------------------------------------------------------------*/
#include "my1image_file.h"
/*----------------------------------------------------------------------------*/
#include <string.h> /** image_write needs string functions */
#include <stdlib.h> /** malloc/free */
/*----------------------------------------------------------------------------*/
#include "my1image_file_bmp.h"
#include "my1image_file_pnm.h"
#ifndef __MY1IMAGE_FILE_BASIC__
#include "my1image_file_png.h"
#endif
/*----------------------------------------------------------------------------*/
static my1image_file_t image_file;
/*----------------------------------------------------------------------------*/
void image_file_init(void) {
	image_file.flag = 0; /*image_file.rsv0 = 0;*/
	image_file.init = 0x0;
	image_file.last = 0x0;
	/* basic formats */
	image_file_ifmt(ifmt_pnm());
	image_file_ifmt(ifmt_bmp());
#ifndef __MY1IMAGE_FILE_BASIC__
	image_file_ifmt(ifmt_png());
#endif
}
/*----------------------------------------------------------------------------*/
void image_file_free(void) {
	while (image_file.init) {
		image_file.last = image_file.init;
		image_file.init = image_file.init->next;
		free((void*)image_file.last);
	}
	image_file.init = 0x0;
	image_file.last = 0x0;
}
/*----------------------------------------------------------------------------*/
void image_file_ifmt(my1image_format_t* ifmt) {
	if (!ifmt) return;
	if (image_file.last)
		image_file.last->next = ifmt;
	if (!image_file.init)
		image_file.init = ifmt;
	image_file.last = ifmt;
}
/*----------------------------------------------------------------------------*/
my1image_format_t* ifmt_make(unsigned int uuid, char* extd,
		pimgfile load, pimgfile save) {
	my1image_format_t* temp;
	temp = (my1image_format_t*) malloc(sizeof(my1image_format_t));
	if (temp) {
		temp->uuid = uuid;
		temp->flag = 0;
		strncpy(temp->extd,extd,IMGFMT_NAMESIZE);
		temp->extd[IMGFMT_NAMESIZE-1] = 0x0;
		temp->do_load = load;
		temp->do_save = save;
		temp->next = 0x0;
	}
	return temp;
}
/*----------------------------------------------------------------------------*/
int image_load(my1image_t* image, char *pfilename) {
	FILE* pfile;
	int flag;
	my1image_format_t *that = image_file.init;
	/* open file for read */
	pfile = fopen(pfilename, "r");
	if (!pfile) return FILE_ERROR_OPEN; /* cannot open file */
	while (that) {
		/* do_load should always be available! */
		flag = that->do_load(image,pfile);
		if (flag==FILE_OK) break;
		/* if error other than non-format, we are done! */
		if (!(flag&FILE_NOT_FORMAT)) break;
		/* reset file pointer and try next format */
		fseek(pfile,0,SEEK_SET);
		that = that->next;
	}
	fclose(pfile);
	return flag;
}
/*----------------------------------------------------------------------------*/
int image_save(my1image_t* image, char *pfilename) {
	FILE* pfile;
	int flag, size;
	char *ptest;
	my1image_format_t *that = image_file.init, *find = image_file.init;
	/* open file for read */
	pfile = fopen(pfilename, "wb");
	if (!pfile) return FILE_ERROR_OPEN; /* cannot open file */
	/* get file extension */
	size = strlen(pfilename);
	ptest = &pfilename[size-4];
	/* compare requested file extension */
	while (that) {
		/* make sure do_save exists && compare type */
		if (that->do_save&&ptest[0]=='.'&&!strcmp(&ptest[1],that->extd)) {
			find = that;
			break;
		}
		that = that->next;
	}
	flag = find->do_save(image,pfile);
	fclose(pfile);
	return flag;
}
/*----------------------------------------------------------------------------*/
int image_cdat(my1image_t* image, char *pfilename) {
	int loop;
	FILE *cfile = fopen(pfilename,"wt");
	if(!cfile) return FILE_ERROR_OPEN; /* cannot open file */
	fprintf(cfile,"unsigned char image[%d] = {",image->size);
	/* write data! */
	for (loop=0;loop<image->size;loop++) {
		if (loop%16==0)
			fprintf(cfile,"\n");
		fprintf(cfile,"0x%02X",image->data[loop]);
		if (loop<image->size-1)
			fprintf(cfile,",");
	}
	fprintf(cfile,"};");
	fclose(cfile);
	return 0;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_FILEC__ */
/*----------------------------------------------------------------------------*/
