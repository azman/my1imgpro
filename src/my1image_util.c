/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_UTILC__
#define __MY1IMAGE_UTILC__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1image_crgb.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h> /* for malloc and free? */
#include <string.h> /* strncpy */
/*----------------------------------------------------------------------------*/
void filter_init(my1ipass_t* pass, pfilter_t filter, char* name) {
	strncpy(pass->name,name,FILTER_NAMESIZE);
	pass->uuid = 0;
	pass->flag = FILTER_FLAG_NONE;
	pass->filter = filter;
	pass->doinit = 0x0; /* used in cloning */
	pass->dofree = 0x0;
	pass->doconf = 0x0;
	pass->buffer = 0x0;
	pass->output = 0x0;
	pass->data = 0x0;
	pass->next = 0x0; /* linked list */
	pass->last = 0x0; /* last filter in chain */
	pass->prev = 0x0; /* will be set by the previous filter in exec chain */
}
/*----------------------------------------------------------------------------*/
void filter_free(my1ipass_t* pass) {
	if (pass->dofree)
		pass->dofree(pass);
}
/*----------------------------------------------------------------------------*/
void filter_free_clones(my1ipass_t* pass) {
	my1ipass_t* temp;
	while (pass) {
		temp = pass;
		pass = pass->next;
		filter_free(temp);
		free((void*)temp);
	}
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_insert(my1ipass_t* pass, my1ipass_t* next) {
	next->next = 0x0;
	next->last = 0x0;
	if (!pass) {
		next->last = next;
		return next;
	}
	pass->last->next = next;
	pass->last = next;
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_remove(my1ipass_t* pass, int index, int cloned) {
	int loop = 0;
	my1ipass_t *prev = 0x0, *curr = pass;
	while (curr) {
		if (index==loop) {
			if (prev) {
				prev->next = curr->next;
				if (!prev->next)
					pass->last = prev;
			}
			else {
				pass = curr->next;
				if (pass)
					pass->last = curr->last;
			}
			if (cloned) {
				filter_free(curr);
				free((void*)curr);
			}
			break;
		}
		prev = curr;
		curr = curr->next;
		loop++;
	}
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_search(my1ipass_t* pass, char *name) {
	int size;
	my1ipass_t* find = 0x0;
	while (pass) {
		size = strlen(pass->name);
		if (!strncmp(pass->name,name,size+1)) {
			find = pass;
			break;
		}
		pass = pass->next;
	}
	return find;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_make(char* name, pfilter_t pass) {
	my1ipass_t* that = (my1ipass_t*)malloc(sizeof(my1ipass_t));
	if (that) filter_init(that,pass,name);
	return that;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_idfl(my1ipass_t* pass,
		unsigned int uuid, unsigned int flag) {
	if (pass) {
		pass->uuid = uuid;
		pass->flag = flag;
	}
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_conf(my1ipass_t* pass,
		pfsetup_t fset, pfclean_t fclr, pfdocfg_t fcfg) {
	if (pass) {
		pass->doinit = fset;
		pass->dofree = fclr;
		pass->doconf = fcfg;
	}
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_doinit(my1ipass_t* pass, my1ipass_t* from) {
	if (pass) {
		if (pass->doinit)
			pass->doinit(pass,from);
	}
	return pass;
}
/*----------------------------------------------------------------------------*/
my1ipass_t* filter_cloned(my1ipass_t* pass) {
	my1ipass_t* temp;
	temp = filter_create_base(pass->name,pass->uuid,pass->flag|FILTER_FLAG_COPY,
		pass->filter,pass->doinit,pass->dofree,pass->doconf);
	return filter_doinit(temp,pass);
}
/*----------------------------------------------------------------------------*/
my1image_t* image_filter(my1image_t* data, my1ipass_t* pass) {
	my1image_t* temp;
	my1ibuff_t* buff;
	my1ipass_t* prev = 0x0;
	while (pass) {
		if (prev) {
			if (pass->flag&FILTER_FLAG_MORE) {
				if (prev->flag&FILTER_FLAG_MOFF) {
					pass = pass->next;
					continue;
				}
			}
			pass->prev = prev;
		}
		if (!(pass->flag&FILTER_FLAG_NOEXEC)) {
			temp = pass->output;
			buff = 0x0;
			if (!temp) {
				buff = pass->buffer;
				if (!buff) return 0x0;
				temp = buff->next;
			}
			data = pass->filter(data,temp,pass);
			if (buff&&(data==buff->next)) /* swap only if buff->next is used */
				buffer_swap(buff);
		}
		prev = pass;
		pass = pass->next;
	}
	return data;
}
/*----------------------------------------------------------------------------*/
my1image_t* image_filter_single(my1image_t* data, my1ipass_t* pass) {
	my1image_t buff;
	/* assumes pass is always valid - output MUST BE defined */
	my1image_t* done = pass->output;
	if (!done) return data;
	image_init(&buff);
	if (pass->flag&FILTER_FLAG_GRAY) {
		image_copy(&buff,data);
		image_grayscale(&buff);
		data = &buff;
	}
	done = pass->filter(data,done,pass);
	image_free(&buff);
	return done;
}
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_UTILC__ */
/*----------------------------------------------------------------------------*/
