/*----------------------------------------------------------------------------*/
/** USEFUL PRESETS */
#if defined(__MY1IMAGE_CORE_FILE__)
#include "my1image_00file.c"
#elif defined(__MY1IMAGE_BASIC__)
#include "my1image_00core.c"
#elif defined(__MY1IMAGE_IF_SELF__)
#include "my1image_01work.c"
#elif defined(__MY1IMAGE_DO_APPW__)
#include "my1image_05core.c"
#elif defined(__MY1IMAGE_DO_MAIN__)
#include "my1image_05main.c"
#elif defined(__MY1IMAGE_DO_LIVE__)
#include "my1image_05live.c"
#elif defined(__MY1IMAGE_DO_DEMO__)
#include "my1image_05full.c"
#else
#include "my1image_01full.c"
#endif
/*----------------------------------------------------------------------------*/
