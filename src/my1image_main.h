/*----------------------------------------------------------------------------*/
#ifndef __MY1IMAGE_MAIN_H__
#define __MY1IMAGE_MAIN_H__
/*----------------------------------------------------------------------------*/
#include "my1image_task.h"
/*----------------------------------------------------------------------------*/
#define MY1IMAIN_LOOP_DELAY 10
/*----------------------------------------------------------------------------*/
#include "my1image_appw.h"
#include "my1image_util.h"
#include "my1image_grab.h"
/*----------------------------------------------------------------------------*/
#define DEF_WIDTH 320
#define DEF_HEIGHT 240
/*----------------------------------------------------------------------------*/
#define DEFAULT_WINDOW_LABEL "MY1 Image"
/*----------------------------------------------------------------------------*/
#define IFLAG_OK 0
#define IFLAG_ERROR (~(~0U>>1))
#define IFLAG_ERROR_ARGS (IFLAG_ERROR|0x0100)
#define IFLAG_ERROR_LOAD (IFLAG_ERROR|0x0200)
#define IFLAG_ERROR_LIST1 (IFLAG_ERROR|0x1000)
#define IFLAG_ERROR_LIST2 (IFLAG_ERROR|0x2000)
/*----------------------------------------------------------------------------*/
#define IFLAG_VIDEO_MODE 0x001
#define IFLAG_VIDEO_HOLD 0x002
#define IFLAG_FILTER_RUN 0x020
#define IFLAG_FILTER_CHK 0x040
#define IFLAG_FILTER_EXE 0x080
/*----------------------------------------------------------------------------*/
typedef my1image_appw_t my1iappw_t;
typedef my1image_show_t my1iinfo_t;
typedef my1image_grab_t my1igrab_t;
/*----------------------------------------------------------------------------*/
typedef struct _my1imain_t {
	my1image_t load, *view;
	my1iappw_t iwin;
	my1iinfo_t info;
	my1igrab_t grab; /* grabber function */
	my1ibuff_t buff;
	unsigned int flag, stat;
	int tdel, temp;
	void* dovf; /* menu_item for filter flag */
	my1ipass_t *list, *curr; /* image processing filters */
	/* work tasks */
	my1itask_t init, free, args;
	my1itask_t prep, proc, show;
	/* user data */
	pdata_t data;
} my1imain_t;
/*----------------------------------------------------------------------------*/
/* will use stat for error status... when it's ready :p */
#define IMAIN(im) ((my1imain_t*)im)
#define imain_error(im,ee) IMAIN(im)->stat|=ee
/*----------------------------------------------------------------------------*/
void imain_data(my1imain_t* imain, pdata_t data);
void imain_init(my1imain_t* imain);
void imain_free(my1imain_t* imain);
void imain_args(my1imain_t* imain, int argc, char* argv[]);
void imain_prep(my1imain_t* imain);
void imain_proc(my1imain_t* imain);
void imain_show(my1imain_t* imain,int argc, char* argv[]);
void imain_loop(my1imain_t* imain, int delta_ms);
void imain_menu_filter_enable(my1imain_t *imain, int enable);
void imain_domenu_filters(my1imain_t* imain);
my1ipass_t* imain_filter_dolist(my1imain_t*,my1ipass_t*);
my1ipass_t* imain_filter_doload(my1imain_t*,char*);
my1ipass_t* imain_filter_unload(my1imain_t*,char*);
my1ipass_t* imain_filter_unload_all(my1imain_t*);
my1ipass_t* imain_filter_doload_list(my1imain_t*,char*);
my1ipass_t* imain_filter_doload_file(my1imain_t*,char*);
my1ipass_t* imain_filter_doexec(my1imain_t* data);
/*----------------------------------------------------------------------------*/
#endif /** __MY1IMAGE_MAIN_H__ */
/*----------------------------------------------------------------------------*/
