/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "my1image_00file.c"
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[]) {
	int test, loop;
	int offs, icol, irow;
	char *pname, *psave, *pdata, *pchk;
	my1image_t buff;
	offs = -1; icol = -1; irow = -1;
	pname = 0x0; psave = 0x0; pdata = 0x0;
	for (loop=1,test=0;loop<argc;loop++) {
		if (!strncmp(argv[loop],"--save",7)) {
			if (++loop<argc) psave = argv[loop];
			else printf("** Cannot get save file name! (%d)\n",++test);
		}
		else if (!strncmp(argv[loop],"--cdata",7)) {
			if (++loop<argc) pdata = argv[loop];
			else printf("** Cannot get C file name! (%d)\n",++test);
		}
		else if (!strncmp(argv[loop],"--pixel",8)) {
			if (++loop<argc) {
				offs = atoi(argv[loop]);
				if (offs<0)
					printf("** Invalid pixel offset (%s)! (%d)\n",
						argv[loop],++test);
			}
			else printf("** Cannot get pixel offset! (%d)\n",++test);
		}
		else if (!strncmp(argv[loop],"--pixelxy",10)) {
			if (++loop<argc) {
				if (sscanf(argv[loop],"%d,%d",&icol,&irow)==2) {
					printf("@@ Pixel @(%d,%d) requested!\n",icol,irow);
				}
				else printf("** Invalid pixel coordinate!! (%d)\n",++test);
			}
			else printf("** Cannot get pixel coordinate!! (%d)\n",++test);
		}
		else if (argv[loop][0]=='-')
			printf("** Unknown option '%s'! (%d)\n",argv[loop],++test);
		else {
			if (!pname) pname = argv[loop];
			else printf("** Unknown param '%s'! (%d)\n",argv[loop],++test);
		}
	}
	if (test) return -1;
	if (!pname) {
		pchk = argv[0];
		loop = 0;
		while (pchk[loop]) {
			if (pchk[loop]=='/') {
				pchk = &pchk[loop+1];
				loop = 0;
			}
			loop++;
		}
		printf("\n%s - simple image load/save tool\n",pchk);
		printf("\nUsage:\n  %s [options] <imgfile>\n",pchk);
		printf("\nOptions:\n");
		printf("  --save <name>\n");
		printf("  --cdata <name>\n");
		printf("  --pixel <offset>\n");
		printf("  --pixelxy <x,y>\n\n");
		return 0;
	}
	image_file_init();
	image_init(&buff);
	printf("-- Loading image file '%s'... ",pname);
	if ((test=image_load(&buff,pname))<0)
		printf("error![%x]\n",(unsigned)test);
	else {
		printf("done![%x]\n",(unsigned)test);
		printf("-- File: %s\n",pname);
		printf("-- Size: %d x %d (Size:%d) {Mask:%08X}\n",
			buff.cols,buff.rows,buff.size,buff.mask);
		if (icol>=0&&irow>=0) {
			if (icol<buff.cols&&irow<buff.rows)
				offs = icol + irow * buff.cols;
			else printf("** Invalid coordinate (%d,%d)/(%d,%d)!\n",
				icol,irow,buff.cols,buff.rows);
		}
		if (offs>=0) {
			if (offs<buff.size) {
				if (icol<0||irow<0) {
					icol = offs%buff.cols;
					irow = offs/buff.cols;
				}
				printf("-- Pixel@(%d,%d) = %d (%08x)\n",icol,irow,
					buff.data[offs],buff.data[offs]);
			}
			else printf("** Invalid pixel offset (%d/%d)!\n",offs,buff.size);
		}
		if (psave) {
			printf("-- Saving image data to %s... ",psave);
			if ((test=image_save(&buff,psave))<0) printf("error![%d]\n",test);
			else printf("done!\n");
		}
		if (pdata) {
			printf("Saving C data to %s... ",pdata);
			if ((test=image_cdat(&buff,pdata))<0) printf("error![%d]\n",test);
			else printf("done!\n");
		}
	}
	image_free(&buff);
	image_file_free();
	return 0;
}
/*----------------------------------------------------------------------------*/
