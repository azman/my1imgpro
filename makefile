# makefile for my1imgpro (basic image/vision processing library)
# - using libav & gtk for interfacing & display

TESTIMG = my1image_test
IMGMAIN = my1image.o
IMGNAME = $(subst .o,,$(IMGMAIN))
IMGBASE = my1image_base.o my1image_gray.o my1image_argb.o
IMGBAS3 = $(IMGBASE) my1image_crgb.o
IMGCORE = $(IMGBAS3) my1image_chsv.o
IMGCORE += my1image_draw.o
IMGFILE = my1image_file.o my1image_file_bmp.o
IMGFILE += my1image_file_pnm.o my1image_file_png.o
# $(IMGCORE) + $(IMGFILE) == my1image_00full
IMGUTIL = my1image_area.o my1image_mask.o my1image_buff.o my1image_util.o
IMGMONO = my1image_scan.o my1image_mono.o
IMGWORK = $(IMGUTIL) $(IMGMONO)
IMGWORK += my1image_math.o
IMGWORK += my1image_stat.o my1image_ifdb.o my1image_ifdb_base.o
IMGFULL = $(IMGCORE) $(IMGFILE) $(IMGWORK)
# $(IMGFULL) == my1image_01full
IMGAPPW = my1image_task.o my1image_view.o my1image_appw.o
IMGMNEW = my1image_grab.o my1image_main.o
IMGDATA = my1image_hist.o my1image_data.o
OBJSIMG = $(IMGFULL) $(IMGAPPW) $(IMGMNEW) $(IMGDATA) $(TESTIMG).o
TESTVIS = my1video_test
VISMAIN = my1video.o
VISNAME = $(subst .o,,$(VISMAIN))
VISBASE = my1video_base.o my1video_data.o
OBJSVIS = $(IMGFULL) $(IMGAPPW) $(IMGMNEW) my1libav_grab.o
OBJSVIS += $(VISBASE) $(TESTVIS).o
TESTCHK = chkimage
CHKLOAD = imgload
CHKSIZE = imgsize
CHKCHSV = hsvtest
CHKPEND = imgappend
TOOLLST = $(CHKLOAD) $(CHKSIZE) $(CHKCHSV) $(CHKPEND)
GTKUSED = my1image_view my1image_appw my1image_grab my1image_main
GTKUSED += my1image_hist my1image_data my1video_data
GTKOBJS = $(addsuffix .o,$(GTKUSED))

CFLAGS += -Wall
LFLAGS += -lm
GFLAGC = $(shell pkg-config --cflags gtk+-3.0)
GFLAGL = $(shell pkg-config --libs gtk+-3.0)
AVFLAG = -lavcodec -lavutil -lavformat -lswscale -lavdevice
VFLAGS = -DMY1APP_VERS=\"$(shell date +%Y%m%d)\"
DFLAGS =

RM = rm -f
CC = gcc -c
LD = gcc

ifeq ($(DO_DEBUG),YES)
	CFLAGS += -g -DMY1DEBUG
endif

# override flags for my1image bundle
test: CFLAGS += $(GFLAGC)
test: LFLAGS += $(GFLAGL) $(AVFLAG)
debug: CFLAGS += -g -DMY1DEBUG

.PHONY: main test all image video tools new debug clean

main: image

test: my1image_05live.o $(TESTCHK).o
	$(LD) $(CFLAGS) -o $(TESTCHK) $+ $(LFLAGS)

all: image video test tools

image: $(TESTIMG)

video: $(TESTVIS)

tools: $(TOOLLST)

new: clean main

debug: new

$(CHKLOAD): $(CHKLOAD).o
	$(LD) $(CFLAGS) -o $@ $+ -static
#	strip $(CHKLOAD)

$(CHKSIZE): my1image_00file.o $(CHKSIZE).o
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS)

$(CHKCHSV): my1image_00full.o $(CHKCHSV).o
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS)

$(CHKPEND): my1image_00file.o my1image_area.o $(CHKPEND).o
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS)

$(TESTIMG): $(OBJSIMG)
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS) $(GFLAGL)

$(TESTVIS): $(OBJSVIS)
	$(LD) $(CFLAGS) -o $@ $+ $(LFLAGS) $(GFLAGL) $(AVFLAG)

$(IMGMAIN): src/$(IMGNAME).c src/$(IMGNAME).h
	$(CC) $(CFLAGS) $(DFLAGS) -o $@ $<

$(VISMAIN): src/$(VISNAME).c src/$(VISNAME).h
	$(CC) $(CFLAGS) $(DFLAGS) -o $@ $<

my1image_05%.o: src/my1image_05%.c src/my1image_05%.h
	$(CC) $(CFLAGS) $(DFLAGS) $(GFLAGC) -o $@ $<

$(GTKOBJS): %.o : src/%.c src/%.h
	$(CC) $(CFLAGS) $(DFLAGS) $(GFLAGC) -o $@ $<

%_test.o: src/%_test.c
	$(CC) $(CFLAGS) $(DFLAGS) $(GFLAGC) $(VFLAGS) -o $@ $<

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) $(DFLAGS) -o $@ $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(DFLAGS) -o $@ $<

clean:
	-$(RM) $(TESTIMG) $(TESTCHK) $(TESTVIS) $(TOOLLST) *.o
